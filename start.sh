#!/bin/bash

cd "$(dirname "$0")"

source .env

IMAGE_NAME=mapcrafter-build

if ! docker ps -a --format '{{.Names}}' | grep -Eq "^$CONTAINER_NAME\$" ; then
  echo "Creating container..."
  docker create -it \
                -v $WORLDS_DIR:/worlds:ro \
                -v $OUTPUT_DIR:/output:rw \
                -v $CONFIG_DIR:/config:ro \
                --name $CONTAINER_NAME \
                $IMAGE_NAME \
                sh -c "/opt/mapcrafter/src/mapcrafter -b -c /config/config.cfg"
fi

if docker ps --format '{{.Names}}' | grep -Eq "^$CONTAINER_NAME\$"; then
  echo "Already running"
else
  echo "Starting container..."
  docker start $CONTAINER_NAME
fi
